import java.util.Scanner;

public class KValue {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.printf("Create a list of (whole) numbers separated by spaces : ");

        // reads input as string and parses it to array
        String strs[] = input.nextLine().trim().split("\\s+");
        int array[] = new int[strs.length];

        // converts string array to int array
        for (int i = 0; i < strs.length; i++)
            array[i] = Integer.parseInt(strs[i]);

        // get k value
        System.out.printf("Choose a k value : ");
        int k = input.nextInt();

        System.out.println(addTwoFindKValue(array, k));

        input.close();
    }

    /**
     * Created a function that returns bool to interrupt the program instead of
     * using break or other statements. If two of the numbers in the array add
     * up to the k value, return true, otherwise false.
     * @param array The array that is being parsed.
     * @param k     The value that the array is testing for.
     * @return      true if ask value is creatable.
     */
    public static boolean addTwoFindKValue(int[] array, int k) {
        for (int x = 0; x < array.length; x++) {
            for (int y = x + 1; y < array.length; y++) {
                if (array[x] + array[y] == k)
                    return true;
            }
        }

        return false;
    }
}