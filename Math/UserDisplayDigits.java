import java.util.Scanner;

public class UserDisplayDigits {
    public static void main(String[] args) {
        //declare variables
        int number, temp, digit;
        //declare and instantiate object Scanner   //classes always start with upper case
        Scanner keyboard;  //declare variable keyboard of type Scanner
        keyboard = new Scanner(System.in);  //instantiate object of type Scanner
        //alternative: Scanner keyboard = new Scanner(system.in);
        //prompt user to enter a number
        System.out.print("Enter a four digit number: ");
        //read number from the user
        number = keyboard.nextInt();
        //calculate and display thousand digit
        System.out.println(number + " displayed digit by digit on separate lines: ");
        digit = number / 1000;
        System.out.println(digit);
        //calculate and display the hundred digit
        temp = number;
        temp = temp % 1000;
        digit = temp / 100;
        System.out.println(digit);
        //calculate the tens and the ones digits
        temp = temp % 100;
        //tens digit
        digit = temp / 10;
        System.out.println(digit);
        //ones digit
        digit = temp % 10;
        System.out.println(digit);
 
        //displaying the number in reverse order after showing it on separate lines
        System.out.print(number + " displayed in reverse order is: ");
        //separate the ones digit  //if original number 9876
        digit = number % 10;
        // result 6
        //display the ones digit
        System.out.print(digit);
        number = number / 10;           //result 987
        //separate the tens digit
        digit = number % 10;
        //result 7
        //display the tens digit
        System.out.print(digit);
        //prepare number for hundreds digit
        number = number / 10;           //result 98
        //separate the hundreds digit
        digit = number % 10;
        //display the hundreds digit
        System.out.print(digit);
        //calculate the thousands digit
        digit = number / 10;
        System.out.println(digit);
        System.out.println("Wow... :)");
    }
}