/*
 * An if-else statement 'chain' to determine what to say depending on weight.
 * if the first statement evaluates to false, it'll move onto the next one until one
 * evaluates to true.
 */

public class IfElseMulti {
    public static void main(String[] args) {
        int weight = 150;

        // checks if weight is less/equal than 115.
        if (weight <= 115) {
            System.out.println("Eat 5 banana splits!");
        }

        // if the previous if-statement evaluates to false, it checks this one.
        else if (weight <= 130) {
            System.out.println("Eat a banana split!");
        }

        // if the previous else-if statement evaluates to false, it checks this one.
        else if (weight <= 200) {
            System.out.println("Perfect!");
        }

        // if everything fails, it runs this code
        else {
            System.out.println("Plenty of banana splits have been consumed!");
        }

        // afterwards, the rest of the code is run.
        System.out.println("Rest of Code!");
    }
}