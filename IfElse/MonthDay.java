/* 
 * This program is designed to ask the user to input a month, and when the 
 * input is given the program spits out how many days the month has
 */

 import java.util.Scanner;

 public class MonthDay {
     public static void main(String[] args) {
         Scanner input = new Scanner(System.in);
         
         // get month as an integer
         System.out.println("Type a number between 1 and 12: ");
         int month = input.nextInt();

         // weird if statement that uses boolean operators
         // || means OR
         if (
             month == 1 || month == 3 || month == 5 || month == 7 || month == 8
          || month == 10 || month == 12
         ) {
             System.out.println("Month has 31 days.");
         } else if (month == 2) {
            System.out.println("Month has 28 or 29 days.");
         } else {
            System.out.println("Month has 30 days.");
         }
     }
 }