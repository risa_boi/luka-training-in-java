/*
Write a program that reads the ages of three persons from the user, and decides who is the oldest, and who the youngest person is.
*/
 
import java.util.Scanner;

public class UserAges {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
 
        int age1, age2, age3;
 
        System.out.print("Enter the age of the first person: ");
        age1 = input.nextInt();
 
        System.out.print("Enter the age of the second person: ");
        age2 = input.nextInt();
 
        System.out.print("Enter the age of the third person: ");
        age3 = input.nextInt();
 
        if ((age2 > age1) && (age3 > age1))
            System.out.println("Person one is the youngest");
 
        if ((age1 > age2) && (age3 > age2))
            System.out.println("Person two is the youngest");

        if ((age1 > age3) && (age2 > age3))
            System.out.println("Person three is the youngest");
 
 
        if ((age2 < age1) && (age3 < age1))
            System.out.println("Person one is the oldest");
 
        if ((age1 < age2) && (age3 < age2))
            System.out.println("Person two is the oldest");

        if ((age1 < age3) && (age2 < age3))
            System.out.println("Person three is the oldest");
    }
}