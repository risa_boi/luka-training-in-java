# Luka's Training in Java

Your training in Java begins now! Here's a list of all the things that I've given examples for for you to refer back to. 

# Format

All of the 'chapters' will be their own separate entry in the table of contents below, and each one will have a link to the directory containing all of the examples. 

### Format for Examples (IMPORTANT)

The format for exmaples will be:
- Pseudo-code on top.
- Link to example with working code beneath it.

# Table of Contents

Here is a list to easily get to wherever you need to go:
1. [If-Else](#if-else)
2. [Loops](#loops)
3. [Math](#math)
4. [Strings](#strings)
5. [Switch Statements](#switch)
6. [Object-Oriented Design](#oop)
7. [More Advanced Problems](#map)

# If-Else Statements <a name="if-else"></a>

### Archive Link for If-Else Statements

[If-Else Directory](IfElse/)

---

### Explanation

A set of instructions that only run if the condition in the parentheses is met. The `else` statement only runs if the if statement before it is `false`.

---

### Examples

Here is a basic example of an if-else statement below:
```java
conditionA = 1
conditionB = 1

if (conditionA == conditionB) {
    // run all of the code here.
}

// rest of the program will be run after.
```
[Example 1](IfElse/MainPageExamples/ExOne.java)

---

As shown above, `if` statements do not have to be ended by `else` every single time they are used.

---

`if` statements can also be written as `else if`. This 'continues' the previous `if` statement and adds another condition and another set of rules if the first condition evaluates to `false`.
```java
conditionA = 1
conditionB = 2
conditionC = 1

// this code will be false, as 1 =/= 2.
if (conditionA == conditionB) {
    // run code here.
}

// after the code above evaluates to false, the program moves to this
// if statement and checks it to see if it's true.
else if (conditionA == conditionC) {
    // run code here.
}

// rest of program.
```
[Example 2](IfElse/MainPageExamples/ExTwo.java)

---

# For, While, and Do-While Loops <a name="loops"></a>

### Archive Link for Loops

[Loops Directory](Loops/)

---

### Explanation

A loop is a section of the code that runs multiple times. There are three types of loops in Java: `for`, `while`, and `do-while`. 
They will be explained below.

---

### While Loops

A `while` loop will run the code within the braces as long as the condition inside the parentheses is met. The format for while loops are as follows:
```java
conditionA = 1

while (conditionA == 1) {
    // run code here infinitely, as long as conditionA equals 1.
}

// rest of the code
```
[Example](Loops/MainPageExamples/ExOne.java)

---

# Mathematics <a name="math"></a>

### Archive Link for Math

[Math Directory](Math/)

---

### Explanation

Math in programming is very simple. A large part of programming is, in fact, mathematics-based. Adding, subtracting, multiplying, and dividing, are all tools available to you with the following operators:
```java
+ // add
- // subtract
/ // divide
* // multiply
% // modulus which gets you the remainder of something
```
Here are some examples of using the operators:
```java
int a   = 5 + 6;        // adds two integer values (whole numbers) together
float b = 5.0 - 6.5;    // adds two float values (decimals) together
int c   = 15 % 4;       // gets the remainder of 15 / 4, which is 3
```

---

# Strings <a name="strings"></a>

### Archived Link for Strings

[Strings Directory](Strings/)

---

### Explanation

Strings are the way that programs display and store text. A string is a series of characters (these characters can include numbers and symbols), and they can be added together just the same as numbers can in the mathematics portion. They are extremely helpful for displaying all kinds of information.

Adding strings together is very simple:
```java
String a = "Hello " + "World!";
```
[Example](Strings/MainPageExamples/ExOne.java)

---

# Switch Statements <a name="switch"></a>

### Archive Link for Switch Statements

[Switch Directory](Switches/)

---

### Explanation

Switch statements can replace long if-else statements in certain situations, using the `case` keyword. Switch statements are used when trying to match one variable to a specific value. The `default` keyword is used in the event that nothing matches. `break` ensures that the first match is the only onet that runs. If there were no break statements, the switch statement would run all of the `case` statements that match.

---

# Object-Oriented Design <a name="oop"></a>

### Archive Link for Object-Oriented Programming

[ObjectOriented Directory](ObjectOriented/)

---

### Explanation

Object-Oriented programming (OOP) is a way of creating code that can make handling lots of data EXTREMELY easy. The basis for it is somewhat simple: an object is a collection of data related to each other. The data inside of an object is described in a `class`. For example, you can have an object hold two variables:
```java
public class Foo {
    public int x, y;

    public static void main(String[] args) {
        // creates two *objects* from the *class* Foo
        Foo a = new Foo();
        Foo b = new Foo();

        // assigns variables to the two vars within the two *objects*
        a.x = 1;
        a.y = 2;
        b.x = 3;
        b.y = 4;
        
        // prints the variable x in each object. OUTPUT: "1 3"
        System.out.println(a.x + " " + b.x);
    }
}
```

An object can also hold a set of instructions called a *method* (a function inside of a class). You can call methods to display or edit the data within an object:
```java
public class Ex {
    // creates an integer called 'x' within the object
    public int x;

    // creates a *method* within the object to change the value of 'x'
    public void setX(int number) {
        x = number;
    }

    // prints the value of 'x'
    public String toString() {
        return "X = " + x;
    }

    public static void main(String[] args) {
        // creates a new object called test
        Ex test = new Ex();

        // sets x to 5
        test.setX(5);

        // prints the string returned by the toString() method
        System.out.println(test.toString());
    }
}
```

--- 

# More Advanced Problems <a name="map"></a>

### Archive Link for More Advanced Problems

[More Advanced Problems](MoreAdvancedProblems)

---

### Explanation

These do not cover a specific component of Java. Rather, they take the previously-learned material and
combine topics, creating problems that require multiple steps to solve. The steps in place to solve these problems
is less straightforward than the other problems.

As a result of there being no specific part of Java to learn in this section, there are no examples on the main page,
as there are many ways to combine ideas.
