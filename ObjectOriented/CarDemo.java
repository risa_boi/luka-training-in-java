public class CarDemo {
    public static void main(String[] args) {
        Car myCar = new Car();
        myCar.setCar("Ford", "Fiesta", 2018);
        Car yourCar = new Car();
        yourCar.setCar("ford", "fieste", 2017);
 
        System.out.println(myCar);
        System.out.println(yourCar);
 
        System.out.println();
        if (myCar.equals(yourCar)) {
            System.out.println("Same model and make");
        } else {
            System.out.println("Cars are different. Here is the information: \n" + myCar.toString() + yourCar.toString());
        }
 
    }
}