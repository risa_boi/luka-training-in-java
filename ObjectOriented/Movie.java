public class Movie {
    public String MPAARating, movieName;
    public int ratingTerrible, ratingBad, ratingOK, ratingGood, ratingGreat;

    public void setName(String name) {
        movieName = name;
    }

    public void setRating(String rate) {
        MPAARating = rate;
    }

    public void addRating(int rating) {
        switch (rating) {
            case 1:
            ratingTerrible++;
            break;

            case 2:
            ratingBad++;
            break;

            case 3:
            ratingOK++;
            break;

            case 4:
            ratingGood++;
            break;

            case 5:
            ratingGreat++;
            break;

            default:
            System.out.println("Invalid Input.");
        }
    }

    public double getAverage() {
        return
           (double)((1 * ratingTerrible) + (2 * ratingBad) +
            (3 * ratingOK) + (4 * ratingGood) + (5 * ratingGreat)) /
            (double)(ratingTerrible + ratingBad + ratingOK + ratingGood + ratingGreat);
    }

    public static void main(String[] args) {
        Movie A = new Movie();
        Movie B = new Movie();

        A.setName("Foo");
        B.setName("Bar");
        A.setRating("G");
        B.setRating("PG-13");

        A.addRating(5);
        A.addRating(4);
        A.addRating(4);
        A.addRating(17);
        A.addRating(1);
        A.addRating(1);
        A.addRating(2);

        B.addRating(1);
        B.addRating(1);
        B.addRating(3);
        B.addRating(5);
        B.addRating(4);
        B.addRating(3);
        B.addRating(6);
        B.addRating(2);

        System.out.println(A.getAverage());
        System.out.println(B.getAverage());
    }
}