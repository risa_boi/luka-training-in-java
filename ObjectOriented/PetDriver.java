public class PetDriver {
    public static void main(String[] args) {
        //declare and instantiate object of type Pet
        Pet pet1 = new Pet();   //use Java default constructor
        // in order to invoke a method, we use dot operator
        System.out.println(pet1.toString());
        pet1.setSpeciesBreed("dog", "Dalmation");
        //if you have a toString() method in the class, you can invoke it with just the name of the object instantiated in a System.out.println(pet1);
        System.out.println(pet1);
 
        if (pet1.equals("dog")) {
            System.out.println("This is a dog");
        } else {
            System.out.println("This is a " + pet1.getSpecies());
        }
 
        Pet pet2 = new Pet();
        pet2.setPet("cat", "Persian", 4, 6.2);
        System.out.println(pet2);
 
        if (pet2.equals("cat")) {
            System.out.println("This is a cat");
        } else {
            System.out.println("This is a " + pet2.getSpecies());
        }
 
        System.out.println();
        Pet doggy = new Pet();
        doggy.setPet("dog", "Dalmation", 10, 20.0);
        System.out.println(doggy);
 
        Pet labby = new Pet();
        labby.setPet("dog", "Labrador", 10, 20.0);
        System.out.println(labby);
 
        if (doggy.equals(labby)) {
            System.out.println("These two dogs are the same!");
        } else {
            if (doggy.equals("dog")) {
                System.out.println("This is a " + doggy.getBreed() + " dog!");
            } else {
                System.out.println("Sorry for confusion. It looked like a dog!");
            }
        }
    }
}