class Counter {
    private int counter;

    void setZero() {
        counter = 0;
    }

    void incCounter() {
        counter++;
    }

    void decCounter() {
        if (counter > 0)
            counter--;
    }

    int accessCount() {
        return counter;
    }

    void printCount() {
        System.out.println("The counter is: " + this.counter);
    }
}

public class CounterProblem {
    public static void main(String[] args) {

        Counter tada = new Counter();

        tada.setZero();
        tada.printCount();
        tada.incCounter();
        tada.incCounter();
        tada.incCounter();
        System.out.println(tada.accessCount());

        tada.decCounter();
        tada.printCount();
        tada.setZero();
        tada.decCounter();
        tada.printCount();
    }
}