public class DisplayLeapYear {
    public static void main(String[] args) {
        //display 10 years in row
        int year = 1582;
        int count = 0;
        
        while(year <= 2018) {
            //leap year: divisible by 4 AND not divisible by 100, OR divisible by 400
            if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
                System.out.print(year + " ");
                count++;
                if (count%10 == 0)
                    System.out.println();
            }

            year++;
        }

        System.out.println();
        System.out.println("There are " + count + " leap years between 1582 and 2018!");
    }
}