/* 
 * Uses the randomizer in java. It chooses a random number between -1 and 1
 * and uses that to move one space a hundred times
 */

 import java.util.Random;

 public class Manhattan {
     public static void main(String[] args) {
         System.out.println("(" + randomWalk(100) + ", " + randomWalk(100) + ")");
     }

     public static int randomWalk(int steps) {
         Random rand = new Random();
         int[] array = new int[] { -1, 0, 1 };
         int value = 0;

         for (int i = 0; i <= steps; i++) {
             value += array[rand.nextInt(3)];
         }

         return value;
     }
 }