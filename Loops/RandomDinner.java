public class RandomDinner {
    public static void main(String[] args) {
        int choice;
        for(int day = 1; day <= 7; day++) {
            //generate the choice
            choice = (int)(Math.random() * 4) + 1;
            switch(choice) {
                case 1:
                case 2:
                    System.out.println("For day " + day + " menu choice is " + choice + " - Pizza");
                    break;
                case 3:
                    System.out.println("For day " + day + " menu choice is " + choice + " - Bibimbap");
                    break;
                case 4:
                    System.out.println("For day " + day + " menu choice is " + choice + " - Couscous");
                    break;
            }
        }
    }
}