/*
Pyramid pattern numbers
Write a nested 'for' loop to display:
                                    1
                                1   2   1
                            1   2   4   2   1
                        1   2   4   8   4   2   1
                    1   2   4   8   16  8   4   2   1
                1   2   4   8   16  32  16  8   4   2   1
            1   2   4   8   16  32  64  32  16  8   4   2   1
        1   2   4   8   16  32  64  128 64  32  16  8   4   2   1
*/
 
public class PyramidPattern {
    public static void main(String[] args) {
        //loop for the 8 rows
        for(int row = 0; row < 8; row++) {
            for (int i = 7 - row; i > 0; i--)
                System.out.print("\t");

            for (int j = 0; j < row + 1; j++)
                System.out.print((int)Math.pow(2,j) + "\t");

            if (row > 0)
                for (int k = 0; k < row; k++)
                    System.out.print((int)Math.pow(2, (row - k - 1)) + "\t");
            System.out.println();
        }
 
    }
}