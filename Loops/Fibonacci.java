/* 
 * Fibonacci sequence program.
 */

public class Fibonacci {
    public static void main(String[] args) {
        int value = fib(8);
        System.out.println(value);
    }

    public static int fib(int sequence) {
        int fOld1 = 1, fOld2 = 1;
        int fNew = 0;
        for (int i = 3; i < sequence; i++) {
            fNew = fOld1 + fOld2;
            fOld1 = fOld2;
            fOld2 = fNew;
            System.out.println(fNew);
        }

        return fNew;
    }
}