/* 
 * This will use a nested for loop to do all the times tables
 * from 1-10
 */

 public class TimesTables {
     public static void main(String[] args) {
         /* 
          * a for loop within a for loop is called a nested for loop
          * they are useful for doing certain kinds of tasks extremely
          * quickly, but they are situational
          */

         for (int i = 1; i <= 10; i++) {
             for (int j = 1; j <= 10; j++) {
                // the \t symbol means "tab". it's used for spacing
                // it multiplies i and j together
                System.out.print(i * j + "\t");
             }
                // the \n symol means newline. it's also for spacing
                System.out.print("\n");
         }
     }
 }